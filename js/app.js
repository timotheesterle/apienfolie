let html = "";

//Planètes
$.getJSON("https://swapi.co/api/planets/?format=json", function(data) {
	console.log(data);

	let templatePlanets = `<h1>Planètes</h1>
<ul>
{{#results}}
	<li>{{name}}
		<ul>
			<li>Habitants :</li>
				<ul>
				{{#residents}}
					<li>{{.}}</li>
				{{/residents}}
				</ul>
			<li>Films :</li>
				<ul>
				{{#films}}
					<li>{{.}}</li>
				{{/films}}
				</ul>
		</ul>
	</li>
{{/results}}
</ul>`;

	html += Mustache.to_html(templatePlanets, data);
});

$.getJSON("https://swapi.co/api/people/?format=json", function(data) {
	console.log(data);

	let templatePlanets = `<h1>Personnages</h1>
<ul>
	{{#results}}
		<li>{{name}}
			<ul>
				<li>Films :</li>
				<ul>
				{{#films}}
					<li>{{.}}</li>
				{{/films}}
				</ul>
			</ul>
			<ul>
				<li>Planète d'origine :</li>
				<ul>
				{{#homeworld}}
					<li>{{.}}</li>
				{{/homeworld}}
				</ul>
			</ul>
			<ul>
				<li>Espèce :</li>
				<ul>
				{{#species}}
					<li>{{.}}</li>
				{{/species}}
				</ul>
			</ul>
			<ul>
				<li>Vaisseaux et véhicules :</li>
				<ul>
				{{#vehicles}}
					<li>{{.}}</li>
				{{/vehicles}}
				</ul>
			</ul>
		</li>
	{{/results}}
</ul>`;

	html += Mustache.to_html(templatePlanets, data);
});

$.getJSON("https://swapi.co/api/films/?format=json", function(data) {
	console.log(data);

	let templatePlanets = `<h1>Films</h1>
<ul>
	{{#results}}
		<li>{{title}}
			<ul>
				<li>Personnages :</li>
				<ul>
				{{#characters}}
					<li>{{.}}</li>
				{{/characters}}
				</ul>
			</ul>
			<ul>
				<li>Planètes :</li>
				<ul>
				{{#planets}}
					<li>{{.}}</li>
				{{/planets}}
				</ul>
			</ul>
			<ul>
				<li>Véhicules :</li>
				<ul>
				{{#vehicles}}
					<li>{{.}}</li>
				{{/vehicles}}
				</ul>
			</ul>
			<ul>
				<li>Espèces :</li>
				<ul>
				{{#species}}
					<li>{{.}}</li>
				{{/species}}
				</ul>
			</ul>
		</li>
	{{/results}}
</ul>`;

	html += Mustache.to_html(templatePlanets, data);

	$("#app").append(html);
});

/* // Toulouse
$.getJSON(
	"https://api.openweathermap.org/data/2.5/weather?id=2972315&appid=f684a7617a09b473e3c8059cd51d6e69&units=metric",
	function(data) {
		console.log(data);

		let templateToulouse = `<h1>Toulouse</h1>
Température : {{main.temp}} °C`;
		html += Mustache.to_html(templateToulouse, data);
	}
);

// Carbonne
$.getJSON(
	"https://api.openweathermap.org/data/2.5/weather?id=6431494&appid=f684a7617a09b473e3c8059cd51d6e69&units=metric",
	function(data) {
		console.log(data);

		let templateCarbonne = `<h1>Carbonne</h1>
Latitude : {{coord.lat}}<br>
Longitude : {{coord.lon}}`;

		html += Mustache.to_html(templateCarbonne, data);

		$("#app").append(html);
	}
); */
